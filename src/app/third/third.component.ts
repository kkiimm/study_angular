import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

declare type customType = {
  text: any;
  number: any;
};
@Component({
  selector: 'app-third',
  templateUrl: './third.component.html',
  styleUrls: ['./third.component.css'],
})
export class ThirdComponent implements OnInit {
  constructor() {}

  @Input() boardBool: boolean;
  @Output() sendMyEvent: EventEmitter<any> = new EventEmitter();
  ngOnInit(): void {}
  dataArray: Array<customType> = [];
  logoutFlag: boolean;

  logout() {
    if (this.boardBool) {
      this.logoutFlag = true;
    } else {
      this.logoutFlag = false;
    }
    this.sendMyEvent.emit(this.logoutFlag);
  }
}
