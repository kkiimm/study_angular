import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css'],
})
export class SecondComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
  @Input() loginFlag: boolean;
  @Output() sendMyEvent: EventEmitter<any> = new EventEmitter();

  id = new FormControl('', [Validators.required, Validators.minLength(5)]);
  pw = new FormControl('', [Validators.required, Validators.minLength(4)]);
  styleArray = { wrong_id: false, wrong_pw: false };
  private message: string;
  public get getMessage(): string {
    return this.message;
  }
  public set setMessage(value: string) {
    this.message = value;
  }

  login(): void {
    if (this.id.value == 'admin' && this.pw.value == '1234') {
      this.loginFlag = true;
    } else {
      this.loginFlag = false;
    }
    this.sendMyEvent.emit(this.loginFlag);

    if (this.id.value != 'admin') {
      this.setMessage = 'Wrong ID';
      this.styleArray.wrong_id = true;
    } else if (this.pw.value != '1234') {
      this.setMessage = 'Wrong PW';
      this.styleArray.wrong_pw = true;
    }
  }
}
