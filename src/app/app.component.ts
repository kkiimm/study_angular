import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'testKIM';
  title_button1 = 'title_button1';
  title_button2 = 'title_button2';

  loginBool = true;
  boardBool = false;

  constructor() {
    console.log('testKIM start');
  }
  buttonAlert(buttonName: number) {
    switch (buttonName) {
      case 1:
        alert(this.title_button1);
        return;
      case 2:
        alert(this.title_button2);
        return;
      default:
        alert(buttonName);
        return;
    }
  }
  getLoginFlag(flag: boolean) {
    if (flag) {
      alert('Login Success!!!!!!!!!!!');
      this.loginBool = false;
      this.boardBool = true;
    } else {
      alert('Login Fail!!!!!!!!!!!');
    }
  }
  getLogoutFlag(flag: boolean) {
    if (flag) {
      alert('Logout Success!!!!!!!!!!!');
      this.loginBool = true;
      this.boardBool = false;
    } else {
      alert('Logout Fail!!!!!!!!!!!');
    }
  }
}
